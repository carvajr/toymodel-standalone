ToyModel-standalone

This is a standalone version of the superposition ToyModel of radio emission.
It is based on the superposition of the Askarian and geomagnetic components
of the radio emission along with their theoretical polarization.
This version is not capable of creating toymodels, but only reading an already
existing one and estimating the peak field and polarization anywhere.

Coordinate system: X points East, Y points N and Z up. Origin is at the core on the ground. All antennas are assumed on the ground (Z=0)

This version is an expansion of the toymodel documented here:
https://arxiv.org/abs/1402.3504

The provided programs can be used to create a radio footprint at the ground.

createmap-rotated-nofilter-standalone.cc  (needs ROOT installed, since it creates a TNtuple with the footprint)

Usage:

	createmap-nofilter-standalone maxX (m)  maxY (m) D (m) rotation angle (deg) Escale toymodelfile.bin

where:
	maxX and maxY (m) are the maximum (absolute) value of the antenna coordinates to be created.
	D (m) is the distance between antennas on the map.
	Rotation angle (deg) is used to rotate the toymodel counter-clockwise, e.g. a shower coming from the N and rotated by 45 deg will now come from the NW.
	Escale is an energy scaling factor for the toymodel, e.g. Escale of 2 will double the toymodel shower energy and an Escale of 0.5 will half the energy.
	toymodelfile.bin is a toymodel file (one example is provided: toymodel-p1e19-tet80-phi180-s1.bin)


createmap-rotated-nofilter-standalone-noroot.cc (does not use ROOT, creates an ASCII file)

Usage:

	createmap-nofilter-standalone-noroot maxX (m)  maxY (m) D (m) rotation angle (deg) Escale toymodelfile.bin  (same parameters as the ROOT version above) 


compilation:
	A Makefile is provided

	make createmap-rotated-nofilter-standalone
	make createmap-rotated-nofilter-standalone-noroot

For the non root version one can compile manually:

    	g++ createmap-rotated-nofilter-standalone-noroot.cc -o createmap-rotated-nofilter-standalone-noroot


Output:
	Both programes create a file with antenna coordinates (in m) and the peak electric field (in uV/m)
	X is E, Y is N and Z is Up (always 0 as antennas are assumed at a plane ground)


Also provided is a root file (createmap-rotated-nofilter-orig.root) with a footprint of the example toymodel (not rotated or scaled) for comparison/testing.


Note: The provided programs calculate the total peak electric field. The components of the electric field can be obtained by changing these programs, e.g.:

      E=ant.fE;

can be changed to:

    Ex=ant.fE.x;
    Ey=ant.fE.y;
    E_hor=sqrt(Ex*Ex+Ey*Ey);

to obtain the horizontal component of the electric field.

