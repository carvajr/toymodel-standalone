CC=g++

ROOT-CFLAGS=`root-config --cflags`
ROOT-LIBS=`root-config --libs`


createmap-rotated-nofilter-standalone: createmap-rotated-nofilter-standalone.cc
	$(CC) createmap-rotated-nofilter-standalone.cc $(ROOT-CFLAGS) $(ROOT-LIBS)   -o createmap-rotated-nofilter-standalone

createmap-rotated-nofilter-standalone-noroot: createmap-rotated-nofilter-standalone-noroot.cc
	$(CC) createmap-rotated-nofilter-standalone-noroot.cc  -o createmap-rotated-nofilter-standalone-noroot

