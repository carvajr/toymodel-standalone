/* This is a standalone version of the superposition ToyModel of radio emission
It is based on the superposition of the Askarian and geomagnetic components
of the radio emission along with their theoretical polarization.
This version is not capable of creating toymodels, but only reading an already
existing one and estimating the peak field and polarization anywhere.

Coordinate system: X points East, Y points N and Z up. Origin is at the core on the ground. All antennas are assumed on the ground (Z=0)

This version is an expansion of the toymodel documented here:
https://arxiv.org/abs/1402.3504
*/

#include <math.h> 
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <algorithm>
#include <iomanip>

using namespace std;

struct RefLine{
    vector<float> r;
    vector<float> geo;
    vector<float> ask;
    int nants;
    //Antennas phi angle for this Reference line
    double fRefPhi; //rad (phi=0 is East, pi/2 is N)

    //Minimum and Maximum values of r of this reference line
    //Cannot interpolate beyond these limits
    double fMinR,fMaxR; 

    //Distance RC from Core to Cherenkov ring along RefLine
    double fRC;

    //Interpolation
    double EvalGeo(float RefR)
	{
	    //NOTE: Could use binary search and/or return Geo AND Ask together
	    if(RefR<r[0] || RefR>r[nants-1])
	    {
		cout<<"EvalGeo Error: RefR out of bounds!"<<endl;
		cout<<"RefR="<<RefR<<" MinR="<<fMinR<<" MaxR="<<fMaxR<<endl;
		return -1;
	    }
	    else if(RefR==r[0]) return geo[0];
	    else if (RefR==r[nants-1]) return geo[nants-1];

	    bool found=false;
	    int i=0,pos;
	    while(!found) 
	    {
		if(r[i]>RefR)
		{
		    pos=i-1;
		    found=true;
		}
		i++;
	    }
	    if(pos==nants-1) return geo[nants-1];
	    double y0 = geo[pos], y1 = geo[pos+1];
	    double x0 = r[pos], x1 = r[pos+1];
	    return y0 + (y1-y0) * (RefR-x0)/(x1-x0); 
	}
    double EvalAsk(float RefR)
	{
	    
	    //NOTE: Could use binary search and/or return Geo AND Ask together
	    if(RefR<r[0] || RefR>r[nants-1])
	    {
		cout<<"EvalAsk Error: RefR out of bounds!"<<endl;
		cout<<"RefR="<<RefR<<" MinR="<<fMinR<<" MaxR="<<fMaxR<<endl;
		return -1;
		
	    }
	    else if(RefR==r[0]) return ask[0];
	    else if (RefR==r[nants-1]) return ask[nants-1];
	    
	    bool found=false;
	    int i=0,pos;
	    while(!found) 
	    {
		if(r[i]>RefR)
		{
		    pos=i-1;
		    found=true;
		}
		i++;
	    }
	    double y0 = ask[pos], y1 = ask[pos+1];
	    double x0 = r[pos], x1 = r[pos+1];
	    return y0 + (y1-y0) * (RefR-x0)/(x1-x0); 
	}

    
    
};


struct Vector{
    //Components of vector: Both cartesian and spherical
    double x,y,z,r,theta,phi;

    //Constructors
    Vector(){}
    //c1 is x or r, c2 is y or theta and c3 is z or phi
    //IsCartesian differentiates between cartesian or spherical initialization
    Vector(double c1, double c2, double c3, bool IsCartesian){
	if(IsCartesian) SetCart(c1,c2,c3);
	else SetSpher(c1,c2,c3);
    }
    //Set unit vector from theta and phi
    Vector(double thetain, double phiin){
	SetSpher(1,thetain,phiin);
    }
    //Set vector using cartesian coordinates 
    //Also automatically sets spherical coordinates from cartesian input
    void SetCart(double xin,double yin,double zin)
	{
	    x=xin;
	    y=yin;
	    z=zin;
	    double rho= sqrt(x*x+y*y);
	    r=sqrt(x*x+y*y+z*z);
	    if(rho==0)
	    {
		theta=0;
		phi=0;
	    }
	    else
	    {
		theta=atan2(rho,z);
		phi=atan2(y,x);
		if(phi<0.0) phi+=2.*acos(-1.); //if phi is negative add 2pi
	    }
	}

    //Set vector using spherical coordinates 
    //Also automatically sets cartesian coordinates from spherical input
    void SetSpher(double rin,double thetain,double phiin)
	{
	    r=rin;
	    theta=thetain;
	    phi=phiin;
	    x=r*sin(theta)*cos(phi);
	    y=r*sin(theta)*sin(phi);
	    z=r*cos(theta);
	}

    //Make the vector a unit vector
    void MakeUnit() {SetSpher(1.0,theta,phi);}

    //Multiplies this vector by a scalar 
    void Multiply(double k) {SetCart(x*=k,y*=k,z*=k);}
    //Divides this vector by a scalar
    void Divide(double k) {SetCart(x/=k,y/=k,z/=k);}
    //Add another vector to this one
    void Add(Vector v) {SetCart(x+v.x,y+v.y,z+v.z);}
    //Subtract another vector from this one
    void Subtract(Vector v) {SetCart(x-v.x,y-v.y,z-v.z);}
    
    void Print(){
	double r2d=180./M_PI;
	cout<<"x="<<x<<endl;
	cout<<"y="<<y<<endl;
	cout<<"z="<<z<<endl;
	cout<<"r="<<r<<endl;
	cout<<"theta="<<theta*r2d<<" deg"<<endl;
	cout<<"phi="<<phi*r2d<<" deg"<<endl;
    }

    //Assignment operator
    Vector & operator=(Vector &v)
	{
	    if (this == &v) return *this;
	    x=v.x;
	    y=v.y;
	    z=v.z;
	    r=v.r;
	    theta=v.theta;
	    phi=v.phi;
	    return *this;
	}
};


//Struct Point is just another name for Vector
struct Point: Vector{};


//Overloaded operators for vectors
Vector operator+(Vector v1,Vector v2)
{
    Vector v(v1.x+v2.x, v1.y+v2.y, v1.z+v2.z,true);
    return v;
}

//v=v1-v2
Vector operator-(Vector v1,Vector v2)
{
    Vector v(v1.x-v2.x, v1.y-v2.y, v1.z-v2.z,true);
    return v;
}

//v=k*vin , k is scalar vin is vector
Vector operator  *(double k,Vector vin)
{
    Vector v(k*vin.x,k*vin.y,k*vin.z,true);
    return v;
}



//Dot Product
double DotProd(Vector v1,Vector v2) { return v1.x*v2.x+v1.y*v2.y+v1.z*v2.z;}

//Cross Product
Vector CrossProd(Vector v1,Vector v2)
{
    double x,y,z;
    
    x= ((v1.y*v2.z)-(v1.z*v2.y));
    y =((v1.z*v2.x)-(v1.x*v2.z));
    z =((v1.x*v2.y)-(v1.y*v2.x));
    Vector v(x,y,z,true);
    return v;
}

//Rotate
Vector Rotate(double Theta,double Phi, Vector vIn)
{
  double A[3][3];   // TRANSFORMATION MATRIX
  //C     1ST Line
  A[0][0]=cos(Theta)*cos(Phi);
  A[0][1]=cos(Theta)*sin(Phi);
  A[0][2]=-sin(Theta);
  //C     2ND Line
  A[1][0]=-sin(Phi);
  A[1][1]=cos(Phi);
  A[1][2]=0.0;
  //C     3RD Line
  A[2][0]=sin(Theta)*cos(Phi);
  A[2][1]=sin(Theta)*sin(Phi);
  A[2][2]=cos(Theta);

  //C     TRANSFORMATION [XYZ]v=A*[XYZ]vIn

  double x,y,z;
  x=A[0][0]*vIn.x+A[0][1]*vIn.y+A[0][2]*vIn.z;
  y=A[1][0]*vIn.x+A[1][1]*vIn.y+A[1][2]*vIn.z;
  z=A[2][0]*vIn.x+A[2][1]*vIn.y+A[2][2]*vIn.z;
  Vector v(x,y,z,true);
  return v;
}

//Rotate Back (inverse rotation)
Vector RotateBack(double Theta,double Phi, Vector vIn)
{
  double A[3][3];   // TRANSFORMATION MATRIX
  //C     1ST COLUMN
  A[0][0]=cos(Theta)*cos(Phi);
  A[1][0]=cos(Theta)*sin(Phi);
  A[2][0]=-sin(Theta);
  //C     2ND COLUMN
  A[0][1]=-sin(Phi);
  A[1][1]=cos(Phi);
  A[2][1]=0.0;
  //C     3RD COLUMN
  A[0][2]=sin(Theta)*cos(Phi);
  A[1][2]=sin(Theta)*sin(Phi);
  A[2][2]=cos(Theta);

  //C     TRANSFORMATION [XYZ]v=A*[XYZ]vIn

  double x,y,z;
  x=A[0][0]*vIn.x+A[0][1]*vIn.y+A[0][2]*vIn.z;
  y=A[1][0]*vIn.x+A[1][1]*vIn.y+A[1][2]*vIn.z;
  z=A[2][0]*vIn.x+A[2][1]*vIn.y+A[2][2]*vIn.z;
  Vector v(x,y,z,true);
  return v;
}


//returns angle between vec1 and vec2
double VecAngle(Vector vec1,Vector vec2)
{
  double DP; //dot product of the two vectors
  double Mod1,Mod2; //length of the two vectors
  double ModMult; //product of lengths
  double CosAngle; //cosine of the angle between vectors

  //first we calculate the length of the vectors
  Mod1=vec1.r;
  Mod2=vec2.r;
  //then their dot product
  DP=DotProd(vec1,vec2);
  //     WE CALCULATE THE COSINE OF THE ANGLE BETWEEN THEM
  //     TESTING THE DENOMINATOR FIRST

  ModMult=Mod1*Mod2;
  if(ModMult==0.0) 
    {
      ModMult=1.e-199;
    }
  CosAngle=DP/ModMult;
  //     AND CHECK FOR PRECISION ERRORS BEFORE USING DACOS
  if(CosAngle < -1.0) CosAngle=-1.0;
  if(CosAngle > 1.0) CosAngle=1.0;
  return acos(CosAngle);
}


double gcm2tohm(double t)
{
    //transforms vertical thickness t [g/cm**2] in altitude h [km]
    //functions h1km and h2km are the inverse of Linsley parametrization
    // for US standard atmosphere
    double h,hm;
    int par;
    if(t < 0.0) 
    {
	cerr<<"gcm2toh ERROR: atmospheric thickness is negative"<<endl;
	return 0;
    }
    if(t <= 0.00128293)
    {
	h=1.e4*(1.12829e-2-t);
	hm=h*1.e3;
	return hm;//return h in meters
    }
    double a[4]={-1.86556e2 , -9.4919e1 , 6.1289e-1 , 0.0};
    double b[4]={1.2227e3 , 1.1449e3 , 1.3056e3 , 5.4018e2};
    double c[4]={9.9419 , 8.7815 , 6.3614 , 7.7217};
    
    if (t <= 3.0395) par=3;
    else if (t <= 271.6991) par=2;
    else if (t <= 631.1) par=1;
    else if (t <= 2004.7) par=0;
    else
    {
	cerr<<"gcm2toh ERROR: atmospheric thickness above 2004.647"<<endl;
	return 0;
    }
    
    h=c[par]*log(b[par]/(t-a[par]));
    hm=h*1.e3;
    return hm; //h in meters
}

struct ToyAntenna{

    int fId; //Antenna ID (NOT THE SAME AS THE EVENT!!!)
    Vector fE; //Total Electric Field vector
    Vector fAsk; //Askaryan component only 
    Vector fGeo; //Geomagnetic component only
    Vector fAnt; //Point with position of antenna (origin is CORE on ground)
    
    //Core is not at the center of the ellipse!
    //We use another system of coordinates where x is the major axis of ellipse
    //And the origin is the CENTER of the ellipse (not the core)
    //Distance to center of ellipse and angle w.r.t major axis of ellipse

    Vector fAntEll; //Point with position of antenna in ellipse system

    //constructors
    ToyAntenna(){}
    ToyAntenna(int Id,Vector E, Vector Geo,Vector Ask,Vector Ant,Vector AntEll){
	fId=Id;
	fE=E;
	fAsk=Ask;
	fGeo=Geo;
	fAnt=Ant;
	fAntEll=AntEll;
	if( (fE.x != fGeo.x + fAsk.x) ||(fE.y != fGeo.y + fAsk.y) ||  (fE.z != fGeo.z + fAsk.z))
	{
	    cout<<"ToyAntenna constructor error: Total field is not the sum of Askaryan and geomagnetic!"<<endl; 
	    exit(-1);
	}
    }
    
};


//structs for i/o
struct DataBlock{
    
//Geomagnectic toymodel unit vector (for all antennas)
    Vector GeoDir;
//Shower Parameters
    //Note that the string with composition is not here!!
    float Zenith;//rad NEW SYSTEM
    float Azimuth;//rad NEW SYSTEM
    float Energy;//eV
    float Xmax;//g / cm^2
    float XmaxVert;//g / cm^2
    float GroundAltitude; //m

    //B parameters
    float Bint;//Tesla
    float Bzen;//rad NEW SYSTEM
    float Bazi;//rad NEW SYSTEM
    float Bincl;//rad
    float Bdecl;//rad

    //Ellipse parameters

    double ThetaC; //Cherenkov angle at Xmax (rad)

    double R1;  //approximate major axis (to be deleted later)
    double R2;  //approximate minor axis (to be deleted later)

    double A1; //distance from core to "early" ellipse (a1=a-epsilon)
    double A2; //distance from core to "late" ellipse (a2=a+epsilon)

    double Major;   //Major axis
    double Minor;   //Minor axis
    double Epsilon; //Distance from center of ellipse to shower core ("error")
    double D;       //Distnace from Xmax to core along axis;
    
    double EllPhi;  //Phi of major axis of ellipse in the Core system 
                     //(fEllPhi=fAzimuth)

    //Angle between B and V
    double Alpha;

};

/////////////////////////////////////////////////////////////////
//                  ToyModel Class                             //
/////////////////////////////////////////////////////////////////

//Parameters used for refractive index calculation (calcring)
const double R0=325.;
const double kr=-0.1218;

class ToyModel {

public:
  //constructors
  ToyModel():currId(0),
  GeometryIsSet(false), ScaleByDistance(true)
  {
  };

  ToyModel(string filename):currId(0),
  GeometryIsSet(false), ScaleByDistance(true)
  {
    Read(filename);
  };


  // //shower properties
  // void SetPrimary(string _primary) {fPrimary = _primary;};
  // void SetEnergy(float _energy) { fEnergy = _energy;};
  // void SetZenithAngle(float _zen) { fZenith = _zen;};
  // void SetAzimuthAngle(float _azi) { fAzimuth = _azi;};
  // void SetXmax(float _xmax) {fXmax = _xmax; };
  // void SetXmaxVert(float _xmaxVert) {fXmaxVert = _xmaxVert; };
  // void SetGroundAltitude(float _ga) {fGroundAltitude = _ga;};
  
  // string GetPrimary() {return fPrimary;};
  float GetEnergy() { return fEnergy;};
  float GetZenithAngle() {return fZenith;};
  float GetAzimuthAngle() { return fAzimuth;};
  // float GetXmax() {return fXmax;};
  // float GetXmaxVert() {return fXmaxVert;};
  // float GetGroundAltitude() {return fGroundAltitude;};
  
  // //Geomagnetic field properties
  // void SetGeomagneticIntesity(float _intens) {fBint = _intens;};
  // void SetGeomagneticInclination(float _inclin) {fBincl = _inclin;};
  // void SetGeomagneticDeclination(float _declin) {fBdecl = _declin;};
  // void SetGeomagneticAzimuth(float _azi) {fBazi = _azi;};
  // void SetGeomagneticZenith(float _zen) {fBzen = _zen;};
  
  // float GetGeomagneticIntesity() {return fBint;};
  // float GetGeomagneticInclination() {return fBincl;};
  // float GetGeomagneticDeclination() {return fBdecl;};
  // float GetGeomagneticAzimuth() {return fBazi;};
  // float GetGeomagneticZenith() {return fBzen;};
  
  
  int GetNAntennas() {return fAntArray.size();};
  
  //new Early-late correction
  bool GetScaleByDistance() {return ScaleByDistance;};
  void SetScaleByDistance(bool flag) {ScaleByDistance=flag;};
  
  // //Ellipse parameters
  // float GetThetaC() {return fThetaC;};
  // float GetR1() {return fR1;};
  // float GetR2() {return fR2;};
  // float GetA1() {return fA1;};
  // float GetA2() {return fA2;};
  // float GetMajor() {return fMajor;};
  // float GetMinor() {return fMinor;};
  // float GetEpsilon() {return fEpsilon;};
  // float GetD() {return fD;};
  // float GetEllPhi() {return fEllPhi;};
  // float GetAlpha() {return fAlpha;};
  
  bool GetGeometryIsSet() {return GeometryIsSet;}
  
  
  //These will store the "reference" absolute value signal line
  //for the pure Askaryan and pure geomagnetic contributions
  RefLine fRefLine;
  
  //Geomagnectic toymodel unit vector (for all antennas)
  Vector fGeoDir;
  
    
  vector<ToyAntenna> fAntArray;
  
  

  void SayHi() {cout<<"Hi! I'm the Interpolation ToyModel!"<<endl;};
  
  bool TestAntenna(Vector antpos)
  {
    double refr= CalcRefR(antpos);
    if(refr<fRefLine.r[0] || refr>fRefLine.r[fRefLine.nants-1]) return false;
    
    else if(refr <= fRefLine.fMaxR-fEpsilon && refr >= fRefLine.fMinR) return true;
    else return false;
  };

  int CreateAntenna(Vector antpos)
  {
    if(!GeometryIsSet)
      {
	cout<<"CreateAntenna Error: Geometry not set yet!"<<endl;
	exit(-1);
      }
     
    //IMPORTANT: For now will assume core is at the center of the ellipse!
    //Question: Do we still assume that? I think not!
    
    //Rotate antpos to axis system
    Vector VantR=Rotate(fZenith,fAzimuth,antpos);
    
    //Calculate Askaryan polarization (radial in axis system)
    double phiR=VantR.phi;
    double xR=-cos(phiR);
    double yR=-sin(phiR);

    //Askaryan polarization vector in axis system is (xR,yR,0)
    Vector VAskDirR(xR,yR,0,true);

    //Rotate VaskR to obtain Askaryan polarization in "normal" system
    Vector VAskDir=RotateBack(fZenith,fAzimuth,VAskDirR);
    VAskDir.MakeUnit(); //make it a unit vector, just in case

        
    //New RefR calculation
    //////
    double refr= CalcRefR(antpos);
    
    //Evaluate askaryan and geomagnetic modulus for this r
    
    double geoabs=fRefLine.EvalGeo(refr);
    double askabs=fRefLine.EvalAsk(refr);
    
    //NEW: scale according to distance to get early-late correction
    if(ScaleByDistance)
      {
	Vector xmax(fD,fZenith,fAzimuth,false);
	Vector pref(refr,M_PI_2,fRefLine.fRefPhi,false);

	
	//old: based on fixed Xmax
	double L0=sqrt( (xmax.x-pref.x)*(xmax.x-pref.x) + (xmax.y-pref.y)*(xmax.y-pref.y) +(xmax.z-pref.z)*(xmax.z-pref.z) );
	double Lant= sqrt ((xmax.x-antpos.x)*(xmax.x-antpos.x) + (xmax.y-antpos.y)*(xmax.y-antpos.y) +(xmax.z-antpos.z)*(xmax.z-antpos.z)  );

	//double scaling=sqrt(L0/Lant);
	double scaling=L0/Lant;
	geoabs*=scaling;
	askabs*=scaling;

      }

    //Test if refr is within bounds (EvalGeo/Ask returns -1 if not)
    if(geoabs == -1)
      {
	cout<<"CreateAntenna ERROR: reference r out of bounds."<<endl;
	return -1;
      }
        
    Vector VAsk=askabs*VAskDir;
    Vector VGeo=geoabs*fGeoDir;
    Vector VE=VAsk+VGeo;
        
    //Transform point to ellipse system
    
    
    Vector antposell=TransformPointToytoEllipse(antpos);

    //TEST: theta should be 90 degrees//////////////
    //except for the core.... 
    //QUESTION: What's de Askaryan polarization AT the core????

    if(( fabs(antposell.theta-M_PI/2.)>1.e-13 ) && (antposell.r != 0) ) 
      {
	cout<<"EEEEEEEEEEEEEEEEEEEEEE"<<endl;
	cout<<" TEST failed: theta!=90 (diff="<<antpos.theta-M_PI/2.<<")"<<endl;
	cout<<"antpos:"<<endl;
	antpos.Print();
	cout<<"antposell:"<<endl;
	antposell.Print();
	cout<<"VAsk:"<<endl;
	VAsk.Print();
	cout<<"EEEEEEEEEEEEEEEEEEEEEE"<<endl;
      }
    ///////////////////////////////////////////////

    //Set new antenna
    ToyAntenna newant(currId,VE,VGeo,VAsk,antpos,antposell);

    fAntArray.push_back(newant);
    currId++;
    return currId-1;
  };
  

  //This calculates the exact R point of the RefLine to be sampled for an antenna
  double CalcRefR(Vector antpos)
  {
    //direction (unit vector) of antenna position toy system
    Vector antdir(antpos.theta,antpos.phi);
    Vector antdirEll=TransformDirToytoEllipse(antdir);
    double rellpoint=REll(antdirEll);
    if(rellpoint==0) return 0;  //Antenna at core. Sample at RefR=0
    
    double rellrefline = fRefLine.fRC;
    double ratioRefPoint = rellrefline/rellpoint;


    //double r2d=180./M_PI;
    return antpos.r*ratioRefPoint;
  };

  //Erases already calculated antennas (NOT FULLY TESTED)
  void ResetAntennas()
  {
    if(GetNAntennas()==0) return;
    vector<ToyAntenna> Temp; //temporary storage. Will get destroyed on return
    Temp.swap(fAntArray); //swap elements with fAntArray
    currId=0;
  };

  //Calculate distance from ellipse to the core from direction in Ell system
  //includes epsilon!
  double REll(Vector dir)
  {
    double R;
    double rx=dir.x;
    double ry=dir.y;
    double rz=dir.z;
    double a=fMajor;
    double b=fMinor;
    double Epx=fEpsilon;

    double A=(a*a*ry*ry+b*b*rx*rx);
    double B=2.0*b*b*Epx*rx;
    double C=(b*b*Epx*Epx-a*a*b*b);
    
    double Delta=B*B-4*A*C;
    if(Delta<0) 
      {
	cout<<"REll ERROR: Delta is Negative!"<<endl;
	exit(-1);
      }
    if(A>0) R=( -B + sqrt(Delta) )/(2*A);
    else 
      {
	R=0;  //if rx=ry=0 (core point) A=0 and R diverges...
      }
    if(R<0) 
      {
	cout<<"REll ERROR: R is negative!"<<endl;
	exit(-1);
      }

    return R;
  };

  ToyAntenna GetAntenna(int Id)
  {
    //TEST
    if(fAntArray[Id].fId != Id) 
      {
	cout<<"GetAntenna ERROR: fAntArray position different than Antenna Id."<<endl;
	exit(-1);
      }
    return fAntArray[Id];
  };

  ////// Transformations between systems
  Vector TransformPointToytoEllipse(Vector point)
  {
    //Tranfromation Point from Toy to ellipse system (contains translation)
    double xe,ye,ze,xt,yt,zt;
    double cosphi=cos(fEllPhi), sinphi=sin(fEllPhi);
    xt=point.x;
    yt=point.y;
    zt=point.z;

    
    xe = xt*cosphi  + yt*sinphi + fEpsilon;
    ye =-xt*sinphi  + yt*cosphi;
    ze = zt;

    Vector pointEll(xe,ye,ze,true);
    return pointEll;
  };

Vector TransformDirToytoEllipse(Vector dir)
  {
    //Tranfromation Point from Toy to ellipse system (contains translation)
    double xe,ye,ze,xt,yt,zt;
    double cosphi=cos(fEllPhi), sinphi=sin(fEllPhi);
    xt=dir.x;
    yt=dir.y;
    zt=dir.z;
    
    xe = xt*cosphi  + yt*sinphi;
    ye =-xt*sinphi  + yt*cosphi;
    ze = zt;

    Vector DirEll(xe,ye,ze,true);
    return DirEll;
  };

  // Vector TransformPointEllipsetoToy(Vector point)
  // {
  //   //Tranforms Point from Ellipse to Toy system (contains translation)
  //   double xe,ye,ze,xt,yt,zt;
  //   double cosphi=cos(fEllPhi), sinphi=sin(fEllPhi);
  //   xe=point.x;
  //   ye=point.y;
  //   ze=point.z;
    
  //   //Is this right? Should I first rotate then add epsilon????
  //   xt = (xe-fEpsilon)*cosphi  - ye*sinphi;
  //   yt = (xe-fEpsilon)*sinphi  + ye*cosphi;
  //   zt = ze;
    
  //   Vector pointToy(xt,yt,zt,true);
  //   return pointToy;
  // };

  // Vector TransformDirEllipsetoToy(Vector dir)
  // {
  //   //Tranfromation Point from Toy to ellipse system (contains translation)
  //   double xe,ye,ze,xt,yt,zt;
  //   double cosphi=cos(fEllPhi), sinphi=sin(fEllPhi);
  //   xe=dir.x;
  //   ye=dir.y;
  //   ze=dir.z;
    
    
  //   xt = xe*cosphi  - ye*sinphi;
  //   yt = xe*sinphi  + ye*cosphi;
  //   zt = ze;
    
  //   Vector DirToy(xt,yt,zt,true);
  //   return DirToy;
  // };

  // void PrintEllipse()
  // {
  //   cout<<"ThetaC="<<fThetaC<<" R1="<<fR1<<" R2="<<fR2<<" A1="<<fA1<<" A2="<<fA2<<endl;
  //   cout<<"Major="<<fMajor<<" Minor="<<fMinor<<" Epsilon="<<fEpsilon<<" EllPhi="<<fEllPhi<<endl;
  // };

  void Read(string filename)
  {
    //Create DataBlock struct to read into
    DataBlock block;   
    
    cout<<"Reading file "<<filename<<"...."<<endl;
    //open input file
    ifstream in;
    in.open(filename.c_str(),  ios::in | ios::binary);
    if(!in) 
      {
	cout<<"ERROR: Could not open file "<<filename<<" !!!"<<endl;
	return;
      }
    

    //read DataBlock from file
    in.read((char*)&block,sizeof(DataBlock));

    //copy block data to toymodel
    fGeoDir=block.GeoDir;
    fZenith=block.Zenith;
    fAzimuth=block.Azimuth;
    fEnergy=block.Energy;
    fXmax=block.Xmax;
    fXmaxVert=block.XmaxVert;
    fGroundAltitude=block.GroundAltitude;
    fBint=block.Bint;
    fBzen=block.Bzen;
    fBazi=block.Bazi;
    fBincl=block.Bincl;
    fBdecl=block.Bdecl;
    fThetaC=block.ThetaC;
    fR1=block.R1;
    fR2=block.R2;
    fA1=block.A1;
    fA2=block.A2;
    fMajor=block.Major;
    fMinor=block.Minor;
    fEpsilon=block.Epsilon;
    fD=block.D;
    fEllPhi=block.EllPhi;
    fAlpha=block.Alpha;

    
    //read string with composition
    int stringsize;
    char cstring[60];
    in.read((char*)&stringsize,sizeof(int));
    in.read( (char*)&cstring,stringsize);
    string compstring(cstring);
    fPrimary=compstring;

    //Serialize RefLine
    int vecsize;
    in.read((char*)&vecsize,sizeof(int)); 
    fRefLine.nants=vecsize;//vecsize is nants!!
    
    float r,ask,geo;
    for(int i=0;i<vecsize;i++)
      {
	in.read((char*)&r,sizeof(float));
	in.read((char*)&ask,sizeof(float));
	in.read((char*)&geo,sizeof(float));
	
	fRefLine.r.push_back(r);
	fRefLine.ask.push_back(ask);
	fRefLine.geo.push_back(geo);

      }

    in.read((char*)&fRefLine.fRefPhi,sizeof(double));
    in.read((char*)&fRefLine.fMinR,sizeof(double));
    in.read((char*)&fRefLine.fMaxR,sizeof(double));
    in.read((char*)&fRefLine.fRC,sizeof(double));

    in.close();

    GeometryIsSet=true;
    cout<<"done!"<<endl;
  };


  //////////////////////////////////////////////////////////////////////
  //NEW: Rotate toymodel to reuse same sims at different zenith angles
  //Rotates the ellipse and RefLine by an angle PSI (clockwise)
  //Resets all antennas
  void RotateToyModel(double Psi) //clockwise in radians
  {
    RotateToyModel(Psi,1.0);
  };

  void RotateToyModel(double Psi, double Escale) //clockwise in radians
  {
    double d2r=M_PI/180.,r2d=180./M_PI;

    cout<<"Will rotate the toymodel by an angle of "<<Psi*r2d<<" deg"<<endl;
    //Reset already calculated antennas
    cout<<"Resetting antennas..."<<endl;
    ResetAntennas();
    
    //change azimuth of shower
    cout<<"Original shower azimuth: "<<fAzimuth*r2d<<" deg"<<endl;
    fAzimuth+=Psi;
    if(fAzimuth >=2.*M_PI) fAzimuth-=2.*M_PI;
    cout<<"New shower azimuth: "<<fAzimuth*r2d<<" deg"<<endl;

    //Recalculate Axis direction
    Vector VDir(fZenith,fAzimuth);
    //V is -Axis Direction ("downward ")
    VDir.Multiply(-1.0);


    //Calculate new angle between B and V
    //save old alpha for the geo scaling
    double sinOldAlpha=sin(fAlpha);
    //B direction
    Vector BDir(fBzen,fBazi);
    fAlpha=VecAngle(BDir,VDir);
      
    //calculate alpha scaling of geomagnetic contribution
    double geoscaling;
    if(sinOldAlpha!=0) geoscaling=sin(fAlpha)/sinOldAlpha;
    else geoscaling=1;
    //cout<<"Geomagnetic scaling due to ToyModel rotation: "<<geoscaling<<endl;

  if(Escale!=1)
    {
      double oldE0=fEnergy;
      fEnergy*=Escale; //newE0=Escale*oldE0
      geoscaling*=Escale;  //include energy scaling on top of alpha dependence
      for(int i=0;i<fRefLine.ask.size();i++) fRefLine.ask[i]*=Escale;
    }
  //scale geomagnetic reference line
  for(int i=0;i<fRefLine.geo.size();i++) fRefLine.geo[i]*=geoscaling;
    
  // new -VxB
  Vector VGeoDir = CrossProd(VDir,BDir);
  VGeoDir.Multiply(-1.0);
  //Make it a unit vector
  VGeoDir.MakeUnit();
  
  //Set new global Geomagnetic polarization vector
  fGeoDir=VGeoDir;

  //Rotate RefLine
  fRefLine.fRefPhi+=Psi;
  if(fRefLine.fRefPhi >=2.*M_PI) fRefLine.fRefPhi-=2.*M_PI;
    
  //change major axis of the ellipse
  //Major axis of ellipse defined as the one in the direction the shower is
  //coming from
  fEllPhi=fAzimuth; //(fAzimuth was already changed above)
  //Note that this changes the TransformDirToytoEllipse and TransformPointToytoEllipse

  //Re-calculate Cherenkov Ring position RC for RefLine (exact)
  //////////////////////////////////
  double RC;
  //Set Ref Line direction unit vector: theta=90, phi=fRefLine.fRefPhi
  Vector RefDir(M_PI_2, fRefLine.fRefPhi);
  //transform direction from toy to ellipse system
  Vector RefDirEll=TransformDirToytoEllipse(RefDir);

  RC=REll(RefDirEll);
  fRefLine.fRC=RC;
  };

  //Shower Parameters
  string fPrimary;//Primary particle as specified in .sry
  float fZenith;//rad NEW SYSTEM
  float fAzimuth;//rad NEW SYSTEM
  float fEnergy;//eV
  float fXmax;//g / cm^2
  float fXmaxVert;//g / cm^2
  float fGroundAltitude; //m
  
  //B parameters
  float fBint;//Tesla
  float fBzen;//rad NEW SYSTEM
  float fBazi;//rad NEW SYSTEM
  float fBincl;//rad
  float fBdecl;//rad
  
  //Ellipse parameters
  
  double fThetaC; //Cherenkov angle at Xmax (rad)
  
  double fR1;  //approximate major axis (to be deleted later)
  double fR2;  //approximate minor axis (to be deleted later)
  
  double fA1; //distance from core to "early" ellipse (a1=a-epsilon)
  double fA2; //distance from core to "late" ellipse (a2=a+epsilon)
  
  double fMajor;   //Major axis
  double fMinor;   //Minor axis
  double fEpsilon; //Distance from center of ellipse to shower core ("error")
  double fD;       //Distnace from Xmax to core along axis;
  
  double fEllPhi;  //Phi of major axis of ellipse in the Core system 
  //(fEllPhi=fAzimuth)
  
  //Angle between B and V
  double fAlpha;
  
    
  
  int currId; //current antenna Id (incremented every time we create an antenna)
  
  bool GeometryIsSet;
  //new early-late correction 
  bool ScaleByDistance;


};
