#include <iostream>
#include "ToyModel-standalone.h"
#include<stdlib.h>
using namespace std;


struct EventAnt
{
    //antenna coordinates (This could be "REAL", reconstructed... whatever) 
    Vector P; 

    //Antenna Id
    int Id;
    
    //Electric field
    Vector E;

    
    void SetP(Vector inpP) {P.SetCart(inpP.x,inpP.y,inpP.z);}

    void SetE(Vector inpE) {E.SetCart(inpE.x,inpE.y,inpE.z);}
    
};

int main(int argc, char** argv) {

  if(argc<7 || argc>7)
    {
	cout<<"Usage: "<<argv[0]<<" maxX (m)  maxY (m) D (m) rotation angle (deg) Escale toymodelfile.bin"<<endl;
	exit(-1);
    }
  
  string inpfile(argv[6]);
  //SayHi();
  cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;
  cout<<"Toymodel input file: "<<inpfile<<endl;
    cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;
    cout<<"creating output file..."<<endl;
    ofstream fout;
    fout.open("createmap-rotated-nofilter-noroot.dat");
    fout<<"# antX antY antZ (m)  E (uV/m)"<<endl;
    cout<<"done!"<<endl;

    float maxx=atof(argv[1]);
    float maxy=atof(argv[2]);
    float D=atof(argv[3]);
    float psideg=atof(argv[4]);
    float escale=atof(argv[5]);
    double Escale=escale;
    double Psi=psideg*acos(-1)/180.;
    cout<<endl;
    cout<<"Array parameters:"<<endl;
    cout<<"maxX="<<maxx<<" m  maxY="<<maxy<<" m  D="<<D<<" m  psideg="<<psideg<<" deg="<<Psi<<" rad  Escale="<<Escale<<endl;
    cout<<endl;

    cout<<"Reading ToyModel...."<<endl;
    ToyModel toy(inpfile);
    cout<<"theta="<<toy.GetZenithAngle()<<"  phi="<<toy.GetAzimuthAngle()<<endl;
    cout<<"done!"<<endl;

    cout<<"Rotating toymodel by "<<psideg<<"degrees clockwise..."<<endl;
    toy.RotateToyModel(Psi,Escale);
    cout<<"done!"<<endl;
    int dummy;
    //cin>>dummy;
    //TNtuple *n=new TNtuple("n","","antx:anty:antz:E");

    Vector Pant;
    Vector E;
    int toyId;
    float x=-maxx,y=-maxy;
    int nentries=0;
    while(x<=maxx)
      {
	while(y<=maxy)
	  {
	    
	    Pant.SetCart(x,y,0);
	    //Pant.Print();
	    //cout<<toy.TestAntenna(Pant)<<endl;
	    if(toy.TestAntenna(Pant))
	      {
		toyId=toy.CreateAntenna(Pant);
		ToyAntenna ant=toy.GetAntenna(toyId);
		E=ant.fE;
		if(E.r>0.e-6) 
		  {
		    cout<<"x="<<x<<"  y="<<y<<" E="<<E.r*1.e6<<" uV/m"<<endl;//E in uV/m
		    fout<<x<<"\t"<<y<<"\t"<<0<<"\t"<<E.r*1.e6<<endl;
		    nentries++;
		  }
	      }
		y+=D;
	    
	  }
	    y=-maxy;
	    x+=D;
      }
    cout<<"E="<<toy.GetEnergy()<<"eV  theta="<<toy.GetZenithAngle()*180./acos(-1)<<"  phi="<<toy.GetAzimuthAngle()*180./acos(-1)<<endl;
    cout<<"nentries="<<nentries<<endl;
    fout.close();
    
    
}
